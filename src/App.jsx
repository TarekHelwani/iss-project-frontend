import './App.css';
import React, { Component } from "react";
import RegisterForm from "./views/registerForm"
import { BrowserRouter, Route, Switch } from "react-router-dom";
import LoginForm from "./views/loginForm";
import Dashboard from "./views/dashboard";
import PasswordForm from "./views/passwordForm";
import ShareRequest from "./views/shareRequst";
import { UserService } from "./services/userService";
import { SocketInstance } from "./utils/socket";
import { PasswordService } from "./services/passwordService";

class App extends Component {

    state = {
        passwords: []
    }

    passwordService = new PasswordService(SocketInstance.getInstance().socket)

    onAdd = async (shareRequest) => {
        await this.passwordService.addPasswordFromShareRequest(shareRequest.password)
        this.setState({})
    }

    onPasswordChange = (passwords) => {
        this.setState({
            passwords: passwords
        })
    }

    render() {
        return (
            <React.Fragment>
                <main className="container">
                    <BrowserRouter>
                        <Switch>
                            <Route
                                path="/register"
                                component={ RegisterForm }
                            />
                            <Route
                                path="/login"
                                component={ LoginForm }
                            />
                            <Route
                                path="/passwords/:id"
                                component={ PasswordForm }
                            />
                            <Route
                                path="/share_requests"
                                render={ (props) => (
                                    <ShareRequest { ...props } onAdd={ this.onAdd }/>
                                ) }
                            />
                            <Route
                                path="/"
                                render={ (props) => (
                                    <Dashboard { ...props }
                                                  passwords={ this.state.passwords }
                                                  onPasswordChange={ this.onPasswordChange }
                                    />
                                ) }
                            />
                        </Switch>
                    </BrowserRouter>
                </main>
            </React.Fragment>
        );
    }

}

export default App;
