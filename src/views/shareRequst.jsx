import { ShareRequestService } from "../services/shareRequestService";
import { SocketInstance } from "../utils/socket";
import { Component } from "react";
import ShareRequestsTable from "./shareRequestsTable";


class ShareRequest extends Component {

    state = {
        shareRequests: [],
    };

    shareRequestService = new ShareRequestService(SocketInstance.getInstance().socket)

    setShareRequests = (shareRequests) => {
        this.setState({
            shareRequests: shareRequests
        })
    }

    async componentDidMount() {
        await this.shareRequestService.getShareRequests(this.setShareRequests)
    }

    handleDelete = async shareRequest => {
        const shareRequests = this.state.shareRequests.filter(m => m._id !== shareRequest._id);
        this.setState({
            shareRequests: shareRequests
        });
    };

    render() {
        return (
            <div className="row">
                <div className="col">
                    { this.renderShareRequests() }
                </div>
            </div>
        );
    }

    renderShareRequests = () => {
        const { onAdd } = this.props
        const { length } = this.state.shareRequests;
        const { shareRequests } = this.state;
        return <div>
            <h3 className="p-5">Showing { length } requests in the database</h3>
            <ShareRequestsTable
                shareRequests={ shareRequests }
                onDelete={ this.handleDelete }
                onAdd = { onAdd }
            />
        </div>
    }
}

export default ShareRequest;