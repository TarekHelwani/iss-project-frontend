import React from "react";
import Form from "../components/form";
import { Link } from "react-router-dom";
import { UserService } from "../services/userService";
import { SocketInstance } from "../utils/socket";
import { cache } from "../utils/cache";

class LoginForm extends Form {

    state = {
        data: {
            email: "",
            password: ""
        },
    }

    userService = new UserService(SocketInstance.getInstance().socket)

    setData = async (user) => {
        if (user) {
            cache.set("currentUser", user)
        }
        this.props.history.push('/', {
            authenticated: !!(await cache.get("currentUser"))
        })
    }

    handleSubmit = async (e) => {
        e.preventDefault()
        await this.userService.login(this.state.data, this.setData)
    }

    render() {
        return (
            <div>
                <h1 className="pt-5">
                    Login
                </h1>
                <form
                    onSubmit={ this.handleSubmit }
                >
                    { this.renderInput('email', 'Email') }
                    { this.renderInput('password', 'Password', 'password') }
                    { this.renderButton("Login") }
                </form>
                <div className="pt-4">
                    Doesn't have account yet? &ensp;
                    <Link underline="none" to="/register">
                        Register
                    </Link>
                </div>
            </div>

        );
    }
}

export default LoginForm;