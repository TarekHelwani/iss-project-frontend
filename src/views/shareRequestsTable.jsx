import React, { Component } from "react";
import Table from "../components/table";

class ShareRequestsTable extends Component {

    columns = [
        { path: 'sender', label: 'Sender' },
        { path: 'password.address', label: 'Address' },
        { path: 'password.password', label: 'Password' },
        {
            key: 'add',
            content: shareRequest => (
                <button
                    onClick={ () => this.props.onAdd(shareRequest) }
                    className="btn btn-success btn-sm"
                >
                    Add
                </button>
            )
        },
        {
            key: 'delete',
            content: shareRequest => (
                <button
                    onClick={ () => this.props.onDelete(shareRequest) }
                    className="btn btn-danger btn-sm"
                >
                    Delete
                </button>
            )
        },
    ]

    render() {
        const { shareRequests } = this.props;
        return (
            <Table
                columns={ this.columns }
                data={ shareRequests }
            />
        );
    }
}

export default ShareRequestsTable;
