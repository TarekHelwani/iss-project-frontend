import React from "react";
import Form from "../components/form";
import { Link } from 'react-router-dom';
import { SocketInstance} from "../utils/socket";
import { UserService } from "../services/userService";
import { cache } from "../utils/cache";

class RegisterForm extends Form {

    state = {
        data: {
            email: "",
            password: ""
        },
    }

    userService = new UserService(SocketInstance.getInstance().socket)

    setData = async (user) => {
        cache.set('currentUser', user)
        this.props.history.push('/', {
            authenticated: true
        })
    }


    handleSubmit = async (e) => {
        e.preventDefault()
        await this.userService.register(this.state.data, this.setData)
    }

    render() {
        return (
            <div>
                <h1 className="pt-5">
                    Register
                </h1>
                <form
                    onSubmit={ this.handleSubmit }
                >
                    { this.renderInput('email', 'Email') }
                    { this.renderInput('password', 'Password', 'password') }
                    { this.renderButton("Register") }
                </form>
                <div className="pt-4">
                    Already have an account ? &ensp;
                    <Link underline="none" to="/login">
                        Login
                    </Link>
                </div>
            </div>
        );
    }
}

export default RegisterForm;