import React from 'react';
import Form from "../components/form";
import { PasswordService } from "../services/passwordService";
import { SocketInstance } from "../utils/socket";
import { sleep } from "../utils/sleep";

class PasswordForm extends Form {

    state = {
        data: {
            address: '',
            email: '',
            password: '',
            description: '',
            attachments: ''
        }
    }

    passwordService = new PasswordService(SocketInstance.getInstance().socket)

    setData = (data) => {
        this.setState({
            data: data
        })
    }

    async populatePasswords() {
        try {
            const passwordId = this.props.match.params.id;
            if (passwordId === "new") return;
            await this.passwordService.find(passwordId, this.setData);
        } catch (ex) {
            if (ex.response && ex.response.status === 404)
                this.props.history.replace('/not-found');
        }
    }

    async componentDidMount() {
        await this.populatePasswords();
    }


    addPassword = async (e) => {
        e.preventDefault()
        await this.passwordService.add(this.state.data)
        sleep(200)
        this.props.history.push("/", {
            authenticated: true
        });
    }

    updatePassword = async (e) => {
        e.preventDefault()
        await this.passwordService.update(this.state.data)
        sleep(200)
        this.props.history.push("/", {
            authenticated: true
        });
    }

    handleFile = e => {
        const file = e.target.files[0]
        let data = {...this.state.data}
        const reader = new FileReader();
        reader.addEventListener("load", () => {
            data.attachments = reader.result
            this.setState({
                data
            });
        }, false)
        if (file) {
            reader.readAsDataURL(file);
        }
    };

    // get the file back from the URL
    dataURLtoFile(dataurl, filename) {
        let arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }


    render() {
        return (
            <div>
                <div>
                    <h1>
                        Password Form
                    </h1>
                    <form
                        onSubmit={ this.props.match.params.id === 'new' ? this.addPassword : this.updatePassword }
                    >
                        { this.renderInput('address', 'Password Address') }
                        { this.renderInput('email', 'Email') }
                        { this.renderInput('password', 'Password') }
                        { this.renderInput('description', 'Description') }
                        <div className="pb-2">
                            <input type="file" onChange={this.handleFile} accept="*" multiple = "{false}"/>
                        </div>
                        { this.renderButton("Save") }
                    </form>
                </div>
            </div>
        );
    }
}

export default PasswordForm;
