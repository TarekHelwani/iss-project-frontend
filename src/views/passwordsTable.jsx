import React, { Component } from "react";
import { Link } from "react-router-dom";
import Table from "../components/table";

class PasswordsTable extends Component {

    columns = [
        {
            key: 'select',
            content: password => (
                <div className="form-check">
                    <input
                        onClick={ () => this.props.onShare(password) }
                        type="checkbox"
                        className="form-check-input"
                        id="exampleCheck1"
                    />
                        <label
                            className="form-check-label"
                            htmlFor="exampleCheck1"
                        >Share</label>
                </div>
            )
        },
        {
            path: 'address',
            label: 'Password Address',
            content: password => <Link to={ `/passwords/${ password._id }` }>{ password.address }</Link>
        },
        { path: 'email', label: 'Email' },
        { path: 'password', label: 'Password' },
        { path: 'description', label: 'Description' },
        {
            key: 'delete',
            content: password => (
                <button
                    onClick={ () => this.props.onDelete(password) }
                    className="btn btn-danger btn-sm"
                >
                    Delete
                </button>
            )
        }
    ]

    render() {
        const { passwords } = this.props;
        return (
            <Table
                columns={ this.columns }
                data={ passwords }
            />
        );
    }
}

export default PasswordsTable;
