import React, { Component } from "react";
import { Link, Redirect, Route } from "react-router-dom";
import PasswordsTable from "./passwordsTable";
import { PasswordService } from "../services/passwordService";
import { SocketInstance } from "../utils/socket";
import { UserService } from "../services/userService";
import ListItem from "../components/listItem"

class Dashboard extends Component {

    state = {
        checkedPassword: {},
        users: []
    };

    passwordService = new PasswordService(SocketInstance.getInstance().socket)
    userService = new UserService(SocketInstance.getInstance().socket)

    setPasswords = (passwords) => {
        const { onPasswordChange } = this.props
        onPasswordChange( passwords )
    }

    setUsers = (users) => {
        this.setState({
            users: users
        })
    }

    async componentDidMount() {
        if (this.props.location.state?.authenticated) {
            await this.passwordService.findAll(this.setPasswords)
        }
        await this.userService.findAll(this.setUsers)
    }

    handleDelete = async password => {
        const passwords = this.state.passwords.filter(m => m._id !== password._id);
        const { onPasswordChange } = this.props
        onPasswordChange( passwords )

        await this.passwordService.remove(password)
    };

    logout = async () => {
        await this.userService.logout()
        this.props.history.push('/')
    }

    checkPassword = async (password) => {
        this.setState({
            checkedPassword: password
        })
    }


    render() {
        const { props } = this
        return (
            <Route
                { ...props }
                render={ props =>
                    props.location.state?.authenticated ?
                        <div className="row">
                            <div className="col">
                                { this.renderPasswords() }
                            </div>
                        </div> :
                        <Redirect to='/register'/>
                }
            />
        );
    }

    renderPasswords = () => {
        const {  users } = this.state;
        const { passwords } = this.props
        const { length } = passwords;
        return <div>
            <div className="dropdown">
                <button
                    className="btn btn-secondary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    Share Password
                </button>
                <div
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                >
                    {
                        users.map(user =>
                            <ListItem
                                user={user}
                                password={this.state.checkedPassword}
                            />
                        )
                    }
                </div>
            </div>

            <Link
                to="/share_requests"
                className="btn btn-primary"
                style={ { marginTop: 20 } }
            >View Share Requests</Link>
            <button
                onClick={ this.logout }
                className="btn btn-dark float-right"
            >
                Log out
            </button>
            <h3 className="p-5">Showing { length } passwords in the database</h3>
            <Link
                to="/passwords/new"
                className="btn btn-primary"
                style={ { marginBottom: 20, marginLeft: 40 } }
            >New Password</Link>
            <PasswordsTable
                passwords={ passwords }
                onDelete={ this.handleDelete }
                onShare= { this.checkPassword }
            />
        </div>
    }
}

export default Dashboard;