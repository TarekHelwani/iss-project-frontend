import {Encryption} from "../security/crypto";
import {Asymmetric} from "../security/asymmetric";
import {SecureThePassword} from "../security/secureThePassword";
import rsa from "js-crypto-rsa";
import {cache, keys} from "../utils/cache";
import {DigitalSignature} from "../security/digitalSignature";
import shareRequst from "../views/shareRequst";

export class RequestManager {

    private readonly encryptionAlgorithm
    private readonly secureThePassword
    private readonly digitalSignature

    constructor(encryptionAlgorithm: Encryption) {
        this.encryptionAlgorithm = encryptionAlgorithm
        this.secureThePassword = new SecureThePassword()
        this.digitalSignature = new DigitalSignature()
    }

    public async init() {
        if (this.encryptionAlgorithm instanceof Asymmetric) {
            await this.encryptionAlgorithm.init()
        }
        await rsa.generateKey(2048).then((key) => {
            keys.set("publicKey", key.publicKey)
            keys.set("privateKey", key.privateKey)
        })
    }

    public isPassword(obj: any) {
        return ( obj.hasOwnProperty("address") || (Array.isArray(obj) && obj.length > 0 && obj[0].hasOwnProperty("address")))
    }

    public newUser() {
        return !cache.get("currentUser")
    }

    public isShareRequest(obj: any) {
        return obj.length > 0 && obj[0].hasOwnProperty("receiver")
    }

    public async encryptThePasswordForTheReceiver(request: any, publicKey: any) {
        return await this.secureThePassword.encrypt(request, publicKey)
    }

    public async decryptPasswordFromShareRequest(password: any) {
        return (await this.secureThePassword.decrypt(password)).data
    }



    public async sendEncryptedData(data: any) {

        console.log('Message to send => ', data)

        // encrypt the password
        if (this.isPassword(data)) {
            data = await this.secureThePassword.encrypt(data)
        }

        data = await this.encryptionAlgorithm.encrypt(data)

        if (! this.newUser()) {
            // Authentication & Authorization (Digital Signature)
            data = await this.digitalSignature.encrypt(data)
        }

        console.log('Message sent => ', data)

        return data
    }

    public async getDecryptedData(data: any) {

        data = (await this.encryptionAlgorithm.decrypt(data)).data

        if (this.isShareRequest(data)) {
            for (let i = 0; i < data.length; i++) {
                data[i] = { ...data[i], password: await this.decryptPasswordFromShareRequest(data[i].password) }
                console.log('data[i] => ', data[i])
            }
        }

        // decrypt the password
        if (this.isPassword(data)) {
            data = (await this.secureThePassword.decrypt(data)).data
        }

        return {
            data: data
        }
    }
}