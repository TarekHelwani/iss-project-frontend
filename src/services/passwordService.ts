import { Socket } from "socket.io-client";
import { ClientEvents } from "../utils/events";
import {Password, PasswordID} from "../utils/entities";
import {requestManager} from "../index";
import {cache} from "../utils/cache";


export class PasswordService {

    private socket: Socket<ClientEvents>;

    constructor(socket: Socket<ClientEvents>) {
        this.socket = socket
    }

    private mapToPassword(object: any): Password {
        let password = {
            _id: object._id  ,
            address: object.address,
            email: object.email,
            password: object.password,
            description: object.description,
            attachments: object.attachments,
            userId: cache.get("currentUser")?._id
        }
        if(!object._id) {
            delete password._id
        }
        return password
    }

    public async findAll(setData: any) {
        this.socket.emit("password:list",  async (res: any) => {
            if ("error" in res) {
                // handle the error
                return;
            }
            setData(await (await requestManager.getDecryptedData(res.data)).data)
        });
    }

    public async find(passwordId: PasswordID, setData: any) {
        let request = {
            passwordId: passwordId,
            userId: cache.get("currentUser")?._id
        }
        this.socket.emit("password:find",  await requestManager.sendEncryptedData(request), async (res: any) => {
            if ("error" in res) {

            }
            setData(await (await requestManager.getDecryptedData(res.data)).data)
        });
    }

    public async remove(object: any) {
        let password = this.mapToPassword(object)
        this.socket.emit("password:delete", await requestManager.sendEncryptedData(password._id), (res: any) => {
            if (res && "error" in res) {
                // handle the error
            }
            console.log(res.data)
        });
    }

    public async add(object: any) {
        let password = this.mapToPassword(object)
        this.socket.emit("password:create", await requestManager.sendEncryptedData(password), (res: any) => {
            if ("error" in res) {
                // handle the error
                return;
            }
            console.log(res.data)
        });
    }

    public async update(object: any) {
        let password = this.mapToPassword(object)
        this.socket.emit("password:update", await requestManager.sendEncryptedData(password), (res: any) => {
            if ("error" in res) {
                // handle the error
                return;
            }
            console.log(res.data)
        });
    }

    public async addPasswordFromShareRequest(password: any) {
        password = this.mapToPassword(password)
        // password = await requestManager.decryptPasswordFromShareRequest(password)
        delete password._id
        this.socket.emit("password:create", await requestManager.sendEncryptedData(password), (res: any) => {
            if ("error" in res) {
                // handle the error
                return;
            }
            console.log(res.data)
        });


    }

}