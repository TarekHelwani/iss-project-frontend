import { Socket } from "socket.io-client";
import { ClientEvents } from "../utils/events";
import { User } from "../utils/entities";
import {requestManager} from "../index";
import {cache, keys} from "../utils/cache";

export class UserService {
    private socket: Socket<ClientEvents>;

    constructor(socket: Socket<ClientEvents>) {
        this.socket = socket
    }

    private static mapToUser(object: any): User {
        let user = {
            _id: object._id  ,
            email: object.email,
            password: object.password,
            publicKey: keys.get("publicKey")
        }
        if(!object._id) {
            delete user._id
        }
        return user
    }

    public async register(object: any, setData: any) {
        const user = UserService.mapToUser(object)

        this.socket.emit("user:register", await requestManager.sendEncryptedData(user), async (res: any) => {
            if (res && "error" in res) {
                // handle the error
            }
            setData(((await requestManager.getDecryptedData(res.data)).data))
        });
    }

    public async login(userInfo: any, setData: any) {
        this.socket.emit("user:login", await requestManager.sendEncryptedData(userInfo), async (res: any) => {
            if (res && "error" in res) {
                setData(null)
                return;
            }
            setData(((await requestManager.getDecryptedData(res.data)).data))
        });
    }

    public logout() {
        cache.clear()
        this.socket.emit("user:logout", (res: any) => {
            if (res && "error" in res) {
                // handle the error
            }
            console.log(res.data)
        });
    }

    public findAll(setData: any) {
        this.socket.emit("user:list",  async (res: any) => {
            if ("error" in res) {
                // handle the error
                return;
            }

            setData(await (await requestManager.getDecryptedData(res.data)).data)
        });
    }
}