import {Socket} from "socket.io-client";
import {ClientEvents} from "../utils/events";
import {requestManager} from "../index";

export class HandshakeService {

    private socket: Socket<ClientEvents>;

    constructor(socket: Socket<ClientEvents>) {
        this.socket = socket
    }

    public async getPublicKey(setData: any) {
        this.socket.emit("handshake:public_key", (res: any) => {
            if ("error" in res) {

            }
            setData(res.data)
        });
    }

    public async sendSessionKey(sessionKey:string) {
        this.socket.emit("handshake:session_key", await requestManager.sendEncryptedData(sessionKey),  (res: any) => {
            if ("error" in res) {

            }
            console.log(res.data)
        });
    }

}