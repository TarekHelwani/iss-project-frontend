import {Socket} from "socket.io-client";
import {ClientEvents} from "../utils/events";
import {ShareRequest} from "../utils/entities";
import {requestManager} from "../index";
import {cache} from "../utils/cache";

export class ShareRequestService {

    private socket: Socket<ClientEvents>;

    constructor(socket: Socket<ClientEvents>) {
        this.socket = socket
    }

    private mapToShareRequest(object: any): ShareRequest {
        let shareRequest = {
            _id: object._id,
            sender: object.sender,
            receiver: object.receiver,
            password: object.password
        }
        if(!object._id) {
            delete shareRequest._id
        }
        return shareRequest
    }

    public async getShareRequests(setData: any) {
        this.socket.emit("shareRequest:list",  await requestManager.sendEncryptedData(cache.get("currentUser")?._id), async (res: any) => {
            if ("error" in res) {
                // handle the error
                return;
            }

            setData(await (await requestManager.getDecryptedData(res.data)).data)
        });
    }

    public async share(password: any, senderId: string, receiverId: string) {

        let requestPublicKey = {
            receiver: receiverId,
            userId: cache.get("currentUser")?._id
        }

        this.socket.emit("shareRequest:public_key", await requestManager.sendEncryptedData(requestPublicKey), async (res: any) => {
            if ("error" in res) {
                // handle the error
                return;
            }

            // get the public key for the receiver ..
            const receiverPublicKey = (await requestManager.getDecryptedData(res.data)).data

            let request = {
                sender: senderId,
                receiver: receiverId,
                // encrypt the password with receiver public key ..
                password: await requestManager.encryptThePasswordForTheReceiver(password, receiverPublicKey),
                userId: cache.get("currentUser")?._id
            }

            this.socket.emit("shareRequest:share", await requestManager.sendEncryptedData(request), (res: any) => {
                if ("error" in res) {
                    // handle the error
                    return;
                }
                console.log(res.data)
            });
        })

    }

}