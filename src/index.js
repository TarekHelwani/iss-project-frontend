import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import { RequestManager } from "./requests/requestManager";
import { Asymmetric } from "./security/asymmetric";
import { cache, keys } from "./utils/cache";

export const requestManager = new RequestManager(new Asymmetric())
requestManager.init()
cache.clear()
keys.clear()

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
