import React, { PureComponent } from "react";
import { PasswordService } from "../services/passwordService";
import { SocketInstance } from "../utils/socket";
import { cache } from "../utils/cache";
import { ShareRequestService } from "../services/shareRequestService";

class ListItem extends PureComponent {

    shareRequestService = new ShareRequestService(SocketInstance.getInstance().socket)

    handleShare = async () => {
        const { user, password } = this.props
        const currentUser = cache.get("currentUser")
        console.log(user)
        console.log(password)

        await this.shareRequestService.share(password, currentUser._id, user._id)
        // do something with index
    }

    render(){
        const { user } = this.props
        return (
            <button className="dropdown-item" onClick={ this.handleShare }>
                { user.email }
            </button>
        )
    }
}

export default ListItem;