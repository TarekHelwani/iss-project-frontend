import {Encryption} from "./crypto";
import {HandshakeService} from "../services/handshakeService";
import {SocketInstance} from "../utils/socket";
import * as crypto from 'crypto'
import { keys } from "../utils/cache";
import rsa from "js-crypto-rsa";
import {AES, enc} from "crypto-ts";


export class Asymmetric implements Encryption {

    private handshakeService = new HandshakeService(SocketInstance.getInstance().socket)
    private publicKey: any
    private sessionKey: any

    setPublicAndSessionKey = async (publicKey: any) => {
        this.publicKey = publicKey
        this.sessionKey = crypto.randomBytes(16).toString('base64')
        await this.handshakeService.sendSessionKey(this.sessionKey)
    }

    async init() {
        await this.handshakeService.getPublicKey(this.setPublicAndSessionKey)
    }


    async encrypt(message: any): Promise<any> {
        const sessionKeyInCache = keys.get('sessionKey')

        //session key hasn't been send to the sever yet
        if (! sessionKeyInCache) {
            console.log('<- <- Handshaking -> -> ')
            console.log('public Key ', this.publicKey)
            console.log('session key ' , message)
            keys.set('sessionKey', message)
            const encoder = new TextEncoder();
            const encodedMessage = encoder.encode(message)
            return await rsa.encrypt(
                encodedMessage,
                this.publicKey,
                'SHA-256', // optional, for OAEP. default is 'SHA-256'
            )
        }
        // Session key has been confirmed
        return AES.encrypt(JSON.stringify(message), this.sessionKey)
    }

    async decrypt(message: any): Promise<any> {
        const bytes = AES.decrypt(message, this.sessionKey);
        return {
            data: JSON.parse(bytes.toString(enc.Utf8))
        }
    }

}