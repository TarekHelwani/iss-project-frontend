export interface Encryption {
    encrypt(message:any): Promise<any>;
    decrypt(message:any): Promise<any>;
}

