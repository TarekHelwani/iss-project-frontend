import {Encryption} from "./crypto";
import {AES, enc} from "crypto-ts";
import {Response} from "../utils/events";
import {cache} from "../utils/cache";
const hash = require('object-hash')

export class Symmetric implements Encryption {

    async encrypt(message: any): Promise<any>{
        const cachedUser = cache.get('currentUser')
        // @ts-ignore
        const key = cachedUser.password
        const request = {
            message: JSON.stringify(message),
            ciphertext: hash.MD5(message)
        }
        return AES.encrypt(JSON.stringify(request), key).toString()
    }

    async decrypt(message: any): Promise<Response<any>>{

        const cachedUser = cache.get('currentUser')
        // @ts-ignore
        const key = cachedUser.password
        const bytes = AES.decrypt(message, key);
        const decryptedData = JSON.parse(bytes.toString(enc.Utf8));
        let {message: decryptedMessage, ciphertext} = decryptedData
        decryptedMessage = JSON.parse(decryptedMessage)
        //check if the mac is the same
        const message_authentication_code = hash.MD5(JSON.stringify(decryptedMessage))
        console.log(message_authentication_code)
        console.log(ciphertext)
        if (message_authentication_code.toString() !== ciphertext.toString()) {
            return {
                error: 'MAC is not the same !'
            }
        }
        return {
            data: decryptedMessage
        }
    }
}