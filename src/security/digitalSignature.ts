import {Encryption} from "./crypto";
import rsa from "js-crypto-rsa";
import {keys} from "../utils/cache";
import {encode} from "base64-arraybuffer";

export class DigitalSignature implements Encryption {

    async encrypt(message: any): Promise<any> {

        const encoder = new TextEncoder()
        message = encoder.encode(message)

        const signature = await rsa.sign(
            message,
            keys.get('privateKey'),
            'SHA-256'
        )

        return {
            message: message,
            signature: signature,
        }
    }

    // this function won't be used
    decrypt(message: any): Promise<any> {
        return Promise.resolve(undefined);
    }
}