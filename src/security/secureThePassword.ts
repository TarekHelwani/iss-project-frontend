import {Encryption} from "./crypto";
import rsa from "js-crypto-rsa";
import {decode, encode} from "base64-arraybuffer";
import {keys} from "../utils/cache";

export class SecureThePassword implements Encryption {

    async encrypt(message: any, publicKey?: any): Promise<any> {

        const encoder = new TextEncoder();
        const decryptedPassword = await rsa.encrypt(
            encoder.encode(message.password),
            publicKey ? publicKey : keys.get('publicKey'),
            'SHA-256',
            // optional, for OAEP. default is 'SHA-256'
        )
        return {...message, password: encode(decryptedPassword)}
    }

    isIterable(obj: any) {
        return Array.isArray(obj)
    }

    async decrypt(message: any): Promise<any> {
        const decoder = new TextDecoder();

        if (!this.isIterable(message)) {
            return {
                data: {
                    ...message, password: decoder.decode(await rsa.decrypt(
                        new Uint8Array(decode(message.password)),
                        keys.get('privateKey'),
                        'SHA-256', // optional, for OAEP. default is 'SHA-256'
                    ))
                }
            }
        }

        return {
            data: Promise.all(
                message.map(async (passwordObject: any) => ({
                    ...passwordObject, password: decoder.decode(await rsa.decrypt(
                        new Uint8Array(decode(passwordObject.password)),
                        keys.get('privateKey'),
                        'SHA-256', // optional, for OAEP. default is 'SHA-256'
                    ))
                })))
        }
    }
}