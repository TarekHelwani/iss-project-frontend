export type PasswordID = string;

export type Password = {
    _id: PasswordID;
    address: string;
    email: string;
    password: string;
    description: string;
    attachments: string;
    userId?: string
}

export type UserID = string;

export type User = {
    _id: UserID;
    email: string;
    password: string;
    publicKey: string
}

export type ShareRequest = {
    sender: string;
    receiver: string;
    password: Object;
}