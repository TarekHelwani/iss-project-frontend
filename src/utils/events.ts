import {Password, ShareRequest, User, UserID} from "./entities";


interface Error {
    error: string;
}

interface Success<T> {
    data: T;
}

export type Response<T> = Error | Success<T>;

export interface ClientEvents {

    /* User Events */
    "user:register": (
        payload: any,
        callback: (res?: Response<User>) => void
    ) => void;

    "user:login": (
        payload: any,
        callback: (res?: Response<User>) => void
    ) => void;

    "user:logout": (
        callback: (res?: Response<string>) => void
    ) => void;

    "user:list": (callback: (res: Response<Password[]>) => void) => void;

    /* Password Events */
    "password:list": (callback: (res: Response<Password[]>) => void) => void;

    "password:create": (
        payload: any,
        callback: (res: Response<string>) => void
    ) => void;

    "password:find": (payload: any, callback: (res: Response<Password>) => void) => void;

    "password:update": (
        payload: any,
        callback: (res?: Response<string>) => void
    ) => void;

    "password:delete": (payload: any, callback: (res?: Response<void>) => void) => void;

    /* Handshaking Events */

    "handshake:public_key": (
        callback: (res?: Response<string>) => void
    ) => void;

    "handshake:session_key": (
        sessionKey:string,
        callback: (res?: Response<string>) => void
    ) => void;

    /* Share Requests Events */

    "shareRequest:list": (
        payload: any,
        callback: (res: Response<ShareRequest[]>) => void
    ) => void;

    "shareRequest:share": (
        payload: any,
        callback: (res?: Response<string>) => void
    ) => void;

    "shareRequest:public_key": (
        payload: any,
        callback: (res?: Response<string>) => void
    ) => void;
}