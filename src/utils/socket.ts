import {io, Socket} from "socket.io-client";
import {ClientEvents} from "./events";

const config = require('../config.json')

export class SocketInstance {

    public socket: Socket<ClientEvents>
    static instance: SocketInstance

    constructor() {
        this.socket = io(config['serverUrl'], {
            transports: ['websocket']
        });
        this.socket.connect()
    }

    static getInstance() {
        if (!this.instance) {
            this.instance = new SocketInstance()
        }
        return this.instance;
    }
}